#!/usr/bin/env bash

docker-compose stop
docker rm -v $(docker ps -aq -f status=exited)
docker-compose build web-server
docker-compose up -d
docker exec -ti zrk_web_server /bin/bash

docker run --rm -it -v $PWD/www/zrk:/var/www ubuntu:16.04 /bin/bash