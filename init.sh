#!/usr/bin/env bash

# Load up .env
set -o allexport
[[ -f .env ]] && source .env
set +o allexport

# Define path variables
rootPath=$PWD
dataPath=$rootPath/data
projectsPath=$rootPath/www

# Set up Git
git config --local user.name $USER_FULL_NAME
git config --local user.email $USER_EMAIL

# Init ZRK project
git clone git@gitlab.com:vlodkow/zrk.git $projectsPath/zrk.loc


