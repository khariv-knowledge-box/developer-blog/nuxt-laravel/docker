FROM ubuntu:16.04

# Ensure UTF-8
RUN apt-get clean && apt-get update && apt-get install -y locales

RUN locale-gen en_US.UTF-8
ENV LANG       en_US.UTF-8
ENV LC_ALL     en_US.UTF-8
ENV TERM xterm

# Nginx-PHP Installation
RUN apt-get update
RUN apt-get install -y \
    mc \
    curl \
    wget \
    git \
    memcached \
    build-essential \
    python-software-properties \
    software-properties-common \
    supervisor

RUN add-apt-repository -y ppa:ondrej/php
RUN add-apt-repository -y ppa:nginx/stable
RUN apt-get update
RUN apt-get install -y --force-yes \
    php7.4-cli \
    php7.4-fpm \
    php7.4-dev \
    php7.4-mysql \
    php7.4-curl \
    # php7.4-memcached \
    php7.4-gd \
    php7.4-mbstring \
    # php7.4-mcrypt \
    php7.4-intl \
    php7.4-tidy \
    php7.4-xml

RUN apt-get install -y nginx

RUN sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.4/fpm/php-fpm.conf
COPY config/php.ini /etc/php/7.4/fpm/php.ini
COPY config/nginx.conf /etc/nginx/nginx.conf
RUN mkdir -p /etc/nginx/sites-available
RUN mkdir -p /etc/nginx/sites-enabled
COPY config/nginx-backend   /etc/nginx/sites-available/backend
RUN ln -sf /etc/nginx/sites-available/backend /etc/nginx/sites-enabled/
RUN mkdir -p /var/log/supervisor
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN mkdir -p /var/run/php
RUN rm -fr /var/www
RUN mkdir -p /var/www

# Get nodeJs + Global pachages
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs
RUN npm install bower gulp -g

# Get Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Configure composer, add asset pachage
RUN composer config -g github-oauth.github.com 2215612cee5d9cf942ea50bea7436af9adc9a04f

# End Nginx-PHP

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /var/www

CMD ["/usr/bin/supervisord", "-n"]