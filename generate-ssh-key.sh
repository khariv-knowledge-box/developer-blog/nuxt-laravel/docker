#!/usr/bin/env bash

# Load up .env
set -o allexport
[[ -f .env ]] && source .env
set +o allexport

publicKeyFile=$HOME/.ssh/id_ed25519

# Generate ssh key
ssh-keygen -t ed25519 -C $USER_EMAIL -N $SSH_PASSPHRASE -f $publicKeyFile

# Copy public key
copiedSuccessfullyMessage="Public Key has been copied. Add your public SSH key to your account"

if [ "$COMMAND_SHELL" == "LINUX" ]; then
    xclip -sel clip < ~/.ssh/id_ed25519.pub
    echo $copiedSuccessfullyMessage

elif [ "$COMMAND_SHELL" == "WINDOWS" ]; then
    cat ~/.ssh/id_ed25519.pub | clip.exe
    echo $copiedSuccessfullyMessage

elif [ "$COMMAND_SHELL" == "MACOS" ]; then
    pbcopy < ~/.ssh/id_ed25519.pub
    echo $copiedSuccessfullyMessage

else
    echo "Command Shell has not found!"
fi